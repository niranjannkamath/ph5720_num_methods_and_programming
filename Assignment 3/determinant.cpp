//this program calculates determinant of a matrix

#include <iostream>
#include <cmath>

using namespace std;

int** minor(int** arr, int N, int p, int q) {											//function to compute minor of an element in a matrix
	int** min;
	min = new int* [N-1];
	for (int i = 0; i < N-1; i++) {
		min[i] = new int[N-1];
	}
	for (int i = 0; i < N; i++) {
		if (i == p) {
			continue;
		}
		for (int j = 0; j < N; j++) {
			if (j == q) {
				continue;
			}
			if (i < p && j < q) {
				min[i][j] = *(arr[i] + j);
			}
			if (i < p && j > q) {
				min[i][j-1] = *(arr[i] + j);
			}
			if (i > p && j < q) {
				min[i-1][j] = *(arr[i] + j);
			}
			if (i > p && j > q) {
				min[i-1][j-1] = *(arr[i] + j);
			}
		}
	}
	return min;
}

int det(int** arr, int N) {																//function to compute determinant
	int determinant = 0;
	int** minorArray;
	if (N == 1) {
		return *arr[0];
	}
	for (int i= 0; i < N; i++) {
		minorArray = minor(arr, N, 0, i);
		determinant += pow(-1, i) * (*(arr[0] + i))*det(minorArray, N-1);
	}
	return determinant;
}


int main() {																			
	int** B = new int*[4];																//initalizing array with pointer
	int C[4][4] = { {1,0,2,-1},{3,0,0,5},{2,1,4,-3},{1,0,5,0} };						//array whose determinant is to be computed

	for (int i = 0; i < 4; i++) {
		B[i] = new int[4];
		for (int j = 0; j < 4; j++) {
			B[i][j] = C[i][j];
		}
	}
	
	cout << det(B, 4);																	//printing result
	
	return 0;
}