//this program multiplies two matrices and prints the result

#include <iostream>

using namespace std;

int** multiply(int** A, int** B, int N) {								//function to multiply two matrices
	int** product = new int* [N];
	for (int i = 0; i < N; i++) {
		product[i] = new int[N];
		for (int j = 0; j < N; j++) {
			product[i][j] = 0;
			for (int k = 0; k < N; k++) {
				product[i][j] += (*(A[i] + k)) * (*(B[k] + j));
			}
		}
	}
	return product;
}

int main() {
	int** A = new int* [4];												//initializing first array				
	int Am[4][4] = { {1,2,3,5}, {4,5,6,3}, {7,8,9,2}, {3,6,5,1} };
	for (int i = 0; i < 4; i++) {
		A[i] = new int[4];
		for (int j = 0; j < 4; j++) {
			A[i][j] = Am[i][j];
		}
	}
	int** B = new int* [4];												//initializing second array
	int Bm[4][4] = { {1,2,3,9}, {4,5,6,5}, {7,8,9,2}, {2,6,9,2} };
	for (int i = 0; i < 4; i++) {
		B[i] = new int[4];
		for (int j = 0; j < 4; j++) {
			B[i][j] = Bm[i][j];
		}
	}
	int** C;															//pointer to array for storing result

	C = multiply(A, B, 4);
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << *(C[i] + j) << " ";									//printing result
		}
		cout << endl;
	}
}