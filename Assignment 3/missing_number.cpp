//this program finds the smallest missing positive number from a given list

#include <iostream>

using namespace std;

int* sort(int* arr, int N) {														//function for sorting the array
	int counter;
	int* sorted = new int[N];
	for (int i = 0; i < N; i++) {
		counter = 0;
		for (int j = 0; j < N; j++) {
			if (*(arr + i) >= *(arr + j)) {
				counter++;
			}
		}
		sorted[counter] = *(arr + i);
	}
	return sorted;
}

int main() {
	int A[8] = { 2,3,-7,6,8,1,-10,15 };												//initializing array
	int* sortedArray;																//array to store sorted array
	int count = 1;
	int j = 0;
	sortedArray = sort((int*)A, 8);
	while(j < 8) {																	//checking to find the missing number
		if (*(sortedArray + j) == count) {
			count++;
			j = 0;
		}
		else if (j == 7) {
			cout << "The smallest missing positive number is : " << count;			//printing missing number
			j++;
		}
		else {
			j++;
		}
	}

	return 0;
}