//this program converts a binary number to decimal
//accept a binary number as input from user
//convert to decimal and print the result

#include <iostream>
#include <cmath>

using std::cin;								//import cin, cout and endl from std library
using std::cout;
using std::endl;

int binaryToDecimal(int bin) {				//function to convert binary to decimal
	int dec = 0;							//variable to store the decimal value
	int remainder = 0;
	int i = 0;								//power counting variable
	while (bin != 0) {
		remainder = bin % 10;
		bin /= 10;
		dec += remainder * pow(2, i);
		i++;
	}
	return dec;
}

int main() {
	int binNum = 0;							//variable to store user input

	cout << "Enter a Binary Number : ";
	cin >> binNum;							//accept a binary input from user

	cout << "The decimal representation for the given binary representation is : " << binaryToDecimal(binNum) << endl;

	return 0;
}