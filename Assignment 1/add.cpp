//This is a program that adds two numbers and prints the sum as output.

#include <iostream>

using std::cin;												//import cin, cout and endl from std library				
using std::cout;
using std::endl;

int main() {
	int num1 = 0;
	int num2 = 0;
	int num = 0;
	cout << "Enter Two Integers : ";						//prompt user to enter two integers
	cin >> num1 >> num2;                                    //accept two integers as input
	num = num1 + num2;										//add two integers and store their sum

	cout << "The Sum of the Numbers is : " << num << endl;

	return 0;
}