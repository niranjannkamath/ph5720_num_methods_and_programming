//this program calculates machine precision
//using this program we calculate the machine precision for singles and doubles
//machine precicion is defined as the largest number x such that 1 + x = 1
//for this we start with x = 1
//then use a while loop with condition 1 + x != 1 we continue to divide x by 2
//print the value of x in the pre-final iteration of the while loop which gives the machine precision

#include <iostream>

using std::cout;							//import cout and endl from std library
using std::endl;

int floatPrecision() {						//function to calculate precision of floats
	float x = 1;							//loop variable
	float y = 0;							//variable to store the result for machine precision
	while (1 + x != 1) {
		y = x;
		x /= 2;
	}
	cout << "The precision of float is : " << y << endl;

	return 0;
}

int doublePrecision() {						//function to calculate presision of doubles
	double x = 1;							//loop variable
	double y = 0;							//variable to store the result for machine precision
	while (1 + x != 1) {
		y = x;
		x /= 2;
	}
	cout << "The precision of double is : " << y << endl;

	return 0;
}

int main() {
	floatPrecision();						//printing precision of floats
	doublePrecision();						//printing precision of doubles

	return 0;
}