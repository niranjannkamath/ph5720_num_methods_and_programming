//this program converts a decimal number to binary
//accept a decimal number as input from user
//convert to binary and print the result

#include <iostream>
#include <cmath>

using std::cin;									//import cin, cout and endl from std library
using std::cout;
using std::endl;

int decimalToBinary(int dec) {					//function to convert decimal to binary
	int bin = 0;								//variable to store the binary value
	int remainder = 0;
	int i = 0;									//power counting variable
	while (dec != 0) {
		remainder = dec % 2;
		dec /= 2;
		bin += remainder * pow(10, i);
		i++;
	}
	return bin;
}

int main() {
	int decNum = 0;							//variable to store user input

	cout << "Enter a decimal Number : ";
	cin >> decNum;							//accept a decimal input from user

	cout << "The binary representation for the given decimal representation is : " << decimalToBinary(decNum) << endl;

	return 0;
}