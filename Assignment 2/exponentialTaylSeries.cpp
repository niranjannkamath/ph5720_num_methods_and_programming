//this program calculates the value of exp(-x) for different values of x
//we use the recursive method for this calculation

#include<iostream>
#include<cmath>
#include<fstream>

using namespace std;

double  exponential(double x, int N) {				//function for computing exponential of x using power series truncated at the (N + 1)th term
	double exp = 1;
	double sn = 1;
	for (int i = 1; i <= N; i++) {
		sn = sn * x / i;
		exp += sn;
	}
	exp = 1 / exp;
	return exp;
}

double relativeErrorExponential(double x, int N) {	//function for calculating relative error in exponential
	double calc = exponential(x, N);
	double actual = exp(-x);
	double err = (calc - actual) / actual;
	double relErr = abs(err);
	return err;
}

int main() {
	double x[4] = { 0.0, 0.0, 0.0, 0.0 };									//argument of the exponential
	double exp = 0;									//result of calculating the exponential
	double relError = 0;							//error in the calculated value w.r.t actual value
	ofstream x_0;									//file stream for writing output

	cout << "Enter Argument of Exponential : ";
	cin >> x[0] >> x[1] >> x[2] >> x[3];										//accept argument of exponential from user
	x_0.open("Xequals0Tayl.txt", ios::trunc);				//open output file "Xequals0Tayl"
	for (int N = 1; N <= 1000; N += 10) {
		exp = exponential(x[0], N);						//calculate exponential
		relError = relativeErrorExponential(x[0], N);		//calculate relative error
		x_0 << N << " " << exp << " " << relError << "\n";	//write output to file
	}
	x_0.close();

	x_0.open("Xequals10Tayl.txt", ios::trunc);				//open output file "Xequals10Tayl"
	for (int N = 1; N <= 1000; N += 10) {
		exp = exponential(x[1], N);						//calculate exponential
		relError = relativeErrorExponential(x[1], N);		//calculate relative error
		x_0 << N << " " << exp << " " << relError << "\n";	//write output to file
	}
	x_0.close();

	x_0.open("Xequals50Tayl.txt", ios::trunc);				//open output file "Xequals50Tayl"
	for (int N = 1; N <= 1000; N += 10) {
		exp = exponential(x[2], N);						//calculate exponential
		relError = relativeErrorExponential(x[2], N);		//calculate relative error
		x_0 << N << " " << exp << " " << relError << "\n";	//write output to file
	}
	x_0.close();

	x_0.open("Xequals100Tayl.txt", ios::trunc);				//open output file "Xequals100Tayl"
	for (int N = 1; N <= 1000; N += 10) {
		exp = exponential(x[3], N);						//calculate exponential
		relError = relativeErrorExponential(x[3], N);		//calculate relative error
		x_0 << N << " " << exp << " " << relError << "\n";	//write output to file
	}
	x_0.close();
	return 0;
}