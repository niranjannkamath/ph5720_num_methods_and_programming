//this program calculates the value of exp(-x) for different values of x
//we use the power series expansion for this function for this calculation

#include<iostream>
#include<cmath>
#include<fstream>

using namespace std;

long double factorial(int N) {								//fuction for computing factorial of an integer N
	long double fact = 1;
	for (int i = 1; i <= N; i++) {
		fact *= i;
	}
	return fact;
}

long double  exponential(double x, int N) {				//function for computing exponential of x using power series truncated at the (N + 1)th term
	long double exp = 1;
	for (int i = 1; i <= N; i++) {
		exp += pow(-x, i)/factorial(i);
	}
	return exp;
}

double relativeErrorExponential(double x, int N) {	//function for calculating relative error in exponential
	double calc = exponential(x, N);
	double actual = exp(-x);
	double err = (calc - actual) / actual;
	double relErr = abs(err);
	return relErr;
}

int main() {
	double x[4] = { 0.0, 0.0, 0.0, 0.0 };									//argument of the exponential
	double exp = 0;									//result of calculating the exponential
	double relError = 0;							//error in the calculated value w.r.t actual value
	ofstream x_0;									//file stream for writing output

	cout << "Enter Argument of Exponential : ";
	cin >> x[0] >> x[1] >> x[2] >> x[3];										//accept argument of exponential from user
	x_0.open("Xequals0Pow.txt", ios::trunc);				//open output file "Xequals0Pow"
	for (int N = 1; N <= 1000; N += 10) {
		exp = exponential(x[0], N);						//calculate exponential
		relError = relativeErrorExponential(x[0], N);		//calculate relative error
		x_0 << N << " " << exp << " " << relError << "\n";	//write output to file
	}
	x_0.close();

	x_0.open("Xequals10Pow.txt", ios::trunc);				//open output file "Xequals10Pow"
	for (int N = 1; N <= 1000; N += 10) {
		exp = exponential(x[1], N);						//calculate exponential
		relError = relativeErrorExponential(x[1], N);		//calculate relative error
		x_0 << N << " " << exp << " " << relError << "\n";	//write output to file
	}
	x_0.close();

	x_0.open("Xequals50Pow.txt", ios::trunc);				//open output file "Xequals50Pow"
	for (int N = 1; N <= 1000; N += 10) {
		exp = exponential(x[2], N);						//calculate exponential
		relError = relativeErrorExponential(x[2], N);		//calculate relative error
		x_0 << N << " " << exp << " " << relError << "\n";	//write output to file
	}
	x_0.close();

	x_0.open("Xequals100Pow.txt", ios::trunc);				//open output file "Xequals100Pow"
	for (int N = 1; N <= 1000; N += 10) {
		exp = exponential(x[3], N);						//calculate exponential
		relError = relativeErrorExponential(x[3], N);		//calculate relative error
		x_0 << N << " " << exp << " " << relError << "\n";	//write output to file
	}
	x_0.close();
	return 0;
}